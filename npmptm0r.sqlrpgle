**free
ctl-opt main(main) dftactgrp(*no);

dcl-f npmptm0w workstn  alias usropn;

dcl-ds NPMP extname('NPMP')  alias  end-ds;

dcl-pr recuperar_texto_error_sql extpgm('NPUTSQER');
    nroerr          packed(4:0);
    sqletx          char(50);
end-pr;

dcl-proc main;
    dcl-pi main;
        parm_modo   char(1);
        parm_nrodoc packed(8:0);
    end-pi;

    
    dcl-s valid ind;
    dcl-s errnbr packed(4:0);
    dcl-s errdsc char(50);

    open npmptm0w;
    select;
        when parm_modo = 'A';
            exsr registro_agregar;
        when parm_modo = '2';
            exsr registro_editar;
        when parm_modo = '4';
            exsr registro_eliminar;
        when parm_modo = '5';
            exsr registro_visualizar;


    endsl;

    exsr finalizar_programa;

    //------------------------------------------------------------
    begsr finalizar_programa;
    //------------------------------------------------------------
        //Cerrar recursos etc etc 
        close npmptm0w;
        
        exec sql commit;

        return;

    endsr;
    //------------------------------------------------------------
    begsr registro_agregar;
    //------------------------------------------------------------
        
        valid = *off;
        *in40 = *off;
        clear P01;
        title='Agregar registro';
        exfmt P01;
        dow *in12 = *off and valid = *off;
            exsr validar_registro;
            if valid;
                exsr insert_record;
                
                if sqlcode =  *zero;
                    leave;
                else;
                    errnbr=sqlcode;
                    recuperar_texto_error_sql(errnbr : errdsc);
                    msgtxt='Error al insertar:(' + %editc(sqlcod:'J') + ') '+ errdsc;
                    exfmt msgbox;
                endif;

            endif;
            exfmt p01;
        enddo;                                         
    
    endsr;

    //-------------------------------------------------------------
    begsr insert_record;
    //-------------------------------------------------------------
        exec sql
            INSERT INTO NPMP (
                NRO_DOCUMENTO, 
                NOMBRE_APELLIDO,        
                NRO_LEGAJO, 
                FEC_NACIMIENTO, 
                FEC_INGRESO, 
                SUELDO_BRUTO, 
                DOMICILIO, 
                CODIGO_POSTAL,
                NRO_TELEFONO             
            )VALUES(
                :NRO_DOCUMENTO, 
                :NOMBRE_APELLIDO,        
                :NRO_LEGAJO, 
                :FEC_NACIMIENTO, 
                :FEC_INGRESO, 
                :SUELDO_BRUTO, 
                :DOMICILIO, 
                :CODIGO_POSTAL,
                :NRO_TELEFONO             
            );
    endsr;
    //------------------------------------------------------------
    begsr registro_editar;
    //------------------------------------------------------------
        title='Editar registro';
        valid = *off;
        *in40 = *off;
        exsr leer_datos;
        exfmt P01;
        dow *in12 = *off and valid = *off;
            exsr validar_registro;
            if valid;
                exsr update_record;
                leave;
            endif;
            exfmt p01;
        enddo;                                         
    endsr;

    //-------------------------------------------------------------
    begsr update_record;
    //-------------------------------------------------------------
        exec sql
            UPDATE NPMP  SET
                 NOMBRE_APELLIDO  = :NOMBRE_APELLIDO
                ,NRO_LEGAJO       = :NRO_LEGAJO
                ,FEC_NACIMIENTO   = :FEC_NACIMIENTO
                ,FEC_INGRESO      = :FEC_INGRESO
                ,SUELDO_BRUTO     = :SUELDO_BRUTO
                ,DOMICILIO        = :DOMICILIO 
                ,CODIGO_POSTAL    = :CODIGO_POSTAL
                ,NRO_TELEFONO     = :NRO_TELEFONO
            WHERE
                NRO_DOCUMENTO = :NRO_DOCUMENTO
        ;
        
    endsr;
    

    //------------------------------------------------------------
    begsr validar_registro;
    //------------------------------------------------------------
        valid = *on;

    endsr;

    //------------------------------------------------------------
    begsr registro_eliminar;
    //------------------------------------------------------------
        title = 'Eliminar Registro';
        exsr leer_datos;    
        *in40 = *on;
        exfmt P01;
        if *in12 = *off;
            exfmt yesno;
            if siono='S';
                exec sql
                        DELETE FROM NPMP MP
                        WHERE
                            MP.NRO_DOCUMENTO = : NRO_DOCUMENTO;          
            endif;
        endif;  
    endsr;

    //------------------------------------------------------------
    begsr registro_visualizar;
    //------------------------------------------------------------
        title = 'Visualizar Registro';
        exsr leer_datos;
        *in40 = *on;
        exfmt p01;               
    endsr;


    //------------------------------------------------------------
    begsr leer_datos;
    //------------------------------------------------------------
        exec sql 
                SELECT  
                    * INTO :NPMP
                FROM NPMP
                WHERE
                        NRO_DOCUMENTO = :parm_nrodoc;

    endsr;

end-proc;
