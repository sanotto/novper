**free
dcl-f npmptc0w workstn sfile(s01:nrr) alias usropn;

dcl-pr npmptm0r extpgm;
    parm_modo    char(1) const;
    parm_nro_doc packed(8:0) const;
end-pr;

dcl-s nrr packed(3:0) inz(*zero);
dcl-c INI_PAG const(-13) ;
dcl-c MAX_LIN const(12) ;
dcl-c RET_PAG const(-12) ;

dcl-ds C1 extname('NPMP') alias end-ds;

open npmptc0w;
exsr abrir_cursor;

exsr cargar_el_subfile;

dow not *in03;
    select;
        when *in40;
            exsr avance_de_pagina;
        when *in41;
            exsr retroceso_de_pagina;
        when *in17;
            exsr ir_al_principio;
        when *in18;
            exsr ir_al_final;
    endsl;

    exsr procesar_selecciones;
    exsr cargar_el_subfile;
enddo;


exsr cerrar_cursor;
close npmptc0w;
return;

//-----------------------------------------------------------------------
begsr cargar_el_subfile;
//-----------------------------------------------------------------------
        *in30 = *off;
        *in31 = *off;
        nrr = *zero;

        write c01;


        exsr fetch_next_c1;
        dow sqlcode = *zero and nrr < MAX_LIN;
            nrr = nrr + 1;
            write s01;
            exsr fetch_next_c1;
        enddo;

        //Voiver el cursor al inic.d/la p�g.x si hay q/recargar la misma
        exec sql
                FETCH RELATIVE :INI_PAG  FROM C1;

        *in31=*on;
        if nrr > 0;
           *in30 = *on;
        endif;

        write f01;
        exfmt c01;
endsr;

//-----------------------------------------------------------------------
begsr avance_de_pagina;
//-----------------------------------------------------------------------
    exec sql
            FETCH RELATIVE :MAX_LIN FROM C1;
endsr;
//-----------------------------------------------------------------------
begsr retroceso_de_pagina;
//-----------------------------------------------------------------------
    exec sql
            FETCH RELATIVE :RET_PAG FROM C1;
endsr;
//-----------------------------------------------------------------------
begsr procesar_selecciones;
//-----------------------------------------------------------------------
    if *in06;
        npmptm0r('A':*zero);
        leavesr;
    endif;

    readc s01;
    dow not %eof();
        npmptm0r(opc:NRO_DOCUMENTO);
        readc s01;
    enddo;

endsr;
//-----------------------------------------------------------------------
begsr abrir_cursor;
//-----------------------------------------------------------------------
    exec sql
            DECLARE C1 DYNAMIC SCROLL CURSOR FOR
            SELECT
                *
            FROM NPMP;

    exec sql
            OPEN C1;
endsr;

//-----------------------------------------------------------------------
begsr cerrar_cursor;
//-----------------------------------------------------------------------
    exec sql
            CLOSE C1;
endsr;

//-----------------------------------------------------------------------
begsr fetch_next_c1;
//-----------------------------------------------------------------------
    exec sql
            FETCH C1 INTO :C1;

endsr;

//-----------------------------------------------------------------------
begsr ir_al_principio;
//-----------------------------------------------------------------------
    exec sql
            FETCH FIRST FROM C1;
    exec sql
            FETCH RELATIVE -1 FROM C1;
endsr;

//-----------------------------------------------------------------------
begsr ir_al_final;
//-----------------------------------------------------------------------
    exec sql
            FETCH AFTER FROM C1;
    exec sql
            FETCH RELATIVE :RET_PAG FROM C1;
endsr;
