# NOVPER - Sistema de Novedades de Personal

## Resumen ejecutivo.

(40 líneas máx que cuenta de que se trata esto)

El sistema de novedades de personal permite registrar
los eventos que le ocurren al personal tales como
carpetas médicas, licencias, capacitaciones etc.
Para ello permite definir de manera dinámica nuevos
tipos de eventos y registrar la asignación de los 
mismos al personal.
Provee asimismo varios mecanismos de consulta acerca
de las novedades registradas.

## Objetivos y limites
 
(Que queremos lograr, que está incluido y que __NO__ lo está)

### Objetivos
(Que queremos lograr)

Proveer al área RR.HH. de un mecanismo para registrar las novedades
de personal, y poder extraer de las mismas información y estádisticas
relevantes para la gestión de dicha aréa

### Límites
(Que se incluye y que no)

El sistema incluye la generación de Listados y planillas de Excell
con distintas consultas necesarias para el sector.
El sistema __NO_incluye interfaz con el sistema de liquidación 
de sueldos.

## Ciclo operativo del Sistema.
(Describir todo el ciclo de vida del sistema en la menor cantidad de líneas posibles)

Se detecta la necesidad de registrar una novedad para un empleado, ya sea por
que esta novedad es informada por el mismo empleado (ej. enfermedad) o por el
departamento de R.R.H.H. (ej. finalización de un curso).
Dicha novedad es registrada en el sistema en un estado "provisional" no se
considera "efectiva" la novedad hasta que la misma no sea "supervisada" por
otra persona distinta a aquella que la creo.
Una vez supervisada la novedad toma el rango de "efectiva" y puede ser utilizada
para la creación de reportes.

## Entidades 

NPCN:   Códigos de novedad: Para registrar los códigos de novedad, existe la posibilidad
        de indicar si se requiere documentación o no (Campo DOC_NECESARIA=true)

NPNP:   Esta tabla contiene las novedades, los campos destacados son FECHA_SUPERVISION  y
        USER_SUPERVISION, los cuales contienen el nombre del usuario que supervisó y la fecha
        de la supervisión si estos campos estan en NULL entonces esta novedad __NO ESTA EN FIRME__.
        Los campos FECHA_LIMITE_PRE_DOC,FECHA_PRES_DOC indican si la documentación fue presentada
        dentro de los plazos establecidos.
.
.
.
.
## Reglas de negocios
(Condiciones que deben cumplirse en el ciclo de vida del sistema)

- Una novedad no se considera "activa" si no está supervisada
- Una novedad no puede ser supervisada por el mismo que la registró.
## Reglas de nombres.
(Reglas para nombrar los objetos)

NORMAS DE NOMBRES

ARCHIVOS FÍSICOS Y LÓGICOS

        NPxxnn	Donde:


        NP	: Fijo indica el sistema al que pertenece el archivo
        xx	: Dos letras que indican el contenido de la tabla ej. MP Maestro de Personal
        nn	: Dos números, comenzando en 01 si y solo si el archivo es un lógico

        Ejemplo:
                        NPMP01:	Maestro de Personal-MP-Por Legajo	

PROGRAMAS

	NPffccnt	Donde:

	NP	: Fijo indica el sistema al que pertenece el programa
	ff	: Letras que indican el archivo principal sobre el que trabaja el programa.
	cc	: Clase o modelo de programa de acuerdo  a la siguiente lista:
			AA	: Alta
			BB	: Consulta
			CC	: Consulta
			MM	: Mantenimiento
			TC	: Trabajar con
			TM	: Trabajar con Mantenimiento
			LS	: Listado
			SF	: Subfile sencillo
                  MM    : Mantenimiento de Maestro
			VS	: Ventana de Selección  
			nn	: Númerico si no es de un tipo especial

	n	: Número de programa si existe más de una instancia de esa clase de programas.
	t	: Indica el tipo del fuente de acuerdo a la siguiente lista:
			R	: RPG
			C	: CLP
			W	: WORKSTATION
			P	: PRINT

	
	Por ejemplo:

		NPMPTC0R	: Novedes de personal Maestro de Personas Trabajar con primer programa tipo RPG
		NPMPTC0W	: Novedes de personal Maestro de Personas Trabajar con primer programa tipo DSPF



## Buenas prácticas.
(Reglas para escribir código elegante y/o correcto - )

- Escribir procedimientos y/o subrutinas de 20 a 40 líneas máximo.
- Elegir nombres descriptivos.


## Programas y procedimientos
(Una lista de programas y/o tareas, con una descripción de lo que deben hacer, 
 quien lo hara y cuanto tiempo llevará, aproximadamente)

            Nombre:	 NPMESF0R	: Novedes de personal Menú                           Responsable:Nacho 
            Nombre:	 NPMPVS0R	: Ventanita p/Sel empleado ACTIVO	             Responsable:Nacho 
            Nombre:	 NPCPVS0R	: Codigos de Postales Selec			     Responsable:Nacho

            Nombre:	 NPMPTC0R	: Maestro de Personal                                Responsable:Eloisa
            Nombre:	 NPMPTC0R	: Maestro de Personal ABM                            Responsable:Eloisa
            Nombre:	 NPNPTM2R	: Supervisar Novedades Activas ABM	             Responsable:Elois

            Nombre:	 NPNPTC1R	: Trabajar con Novedades Hist.		             Responsable:Abril
            Nombre:	 NPCPTC0R	: Codigos Postales                                   Responsable:Abril
            Nombre:	 NPCNTM0R	: Codigos de Postales ABM                            Responsable:Abril

            Nombre:	 NPNPTC0R	: Trabajar con Novedades Activas                     Responsable:Ana
            Nombre:	 NPNPTM0R	: Trabajar con Novedades Activas                     Responsable:Ana
            Nombre:	 NPNPLS0R	: Novedades del dia                                  Responsable:Ana

            Nombre:	 NPCNTC0R	: Codigos de Novedad                                 Responsable:Gonzalo
            Nombre:	 NPCNTM0R	: Codigos de Novedad ABM                             Responsable:Gonzalo
            Nombre:      NPNPLS1C	: Novedades con doc vencida Excel                    Responsable:Gonzalo

            Nombre:	 NPNPLS0C	: Novedades del dia- Excel			     Responsable:Ramiro
            Nombre:	 NPNPVS0R	: Ventanita para selec código                        Responsable:Ramiro
            Nombre:	 NPNP000C	: Recuperar Nro de Novedad			     Responsable:Ramiro
