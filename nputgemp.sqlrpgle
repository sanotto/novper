**free
ctl-opt main(main)  DFTACTGRP(*NO);

dcl-s apellidos   varchar(20)  dim(10) ctdata perrcd(1);
dcl-s nombres     varchar(20)  dim(10) ctdata perrcd(1);

dcl-proc main;

       
    dcl-s NRO_DOCUMENTO     packed(8:0);
    dcl-s NRO_LEGAJO        packed(8:0);
    dcl-s FEC_INGRESO       date;
    dcl-s NOMBRE_APELLIDO   varchar(50);
    dcl-s FEC_NACIMIENTO    date;
    dcl-s SUELDO_BRUTO      packed(15:2);

    dcl-s i  packed(8:0);

    exsr limpiar_tabla;

    for i = 1 to 10000;
        NRO_DOCUMENTO   = generar_nro_documento();
        NOMBRE_APELLIDO = generar_nombre();
        NRO_LEGAJO      = generar_nro_legajo();        
        FEC_NACIMIENTO  = generar_fecha(1970:30);
        FEC_INGRESO     = generar_fecha(2000:15);
        SUELDO_BRUTO    = generar_nro_aleatorios(1:999) *100 +100000;
        exsr insertar_registro;
    endfor;
    

    return ;
    
    //---------------------------------------------------------
    begsr limpiar_tabla;
    //---------------------------------------------------------
        exec sql DELETE FROM NOVPERDB.NPMP;
    endsr;

    //---------------------------------------------------------
    begsr insertar_registro;
    //---------------------------------------------------------
            exec sql INSERT INTO NOVPERDB/NPMP (
                NRO_DOCUMENTO, 
                NOMBRE_APELLIDO,       
                NRO_LEGAJO, 
                FEC_NACIMIENTO, 
                FEC_INGRESO, 
                SUELDO_BRUTO, 
                DOMICILIO,
                CODIGO_POSTAL, 
                NRO_TELEFONO
                ) 
                VALUES
                (   
                      :NRO_DOCUMENTO
                    , :NOMBRE_APELLIDO 
                    , :NRO_LEGAJO 
                    , :FEC_NACIMIENTO
                    , :FEC_INGRESO
                    , :SUELDO_BRUTO
                    , 'SIN DOM' 
                    , 5300 
                    , 0
                );                         
    endsr;

end-proc;

//==========================================================================
dcl-proc generar_nro_legajo;
//==========================================================================
    dcl-pi *n packed(8:0);
    end-pi;

    dcl-s nroleg packed(8:0);

        exec sql    SELECT 
                    INT((RANDOM()*99998)+1)  INTO :nroleg
                FROM sysibm.sysdummy1;
        
        return nroleg;
end-proc;


//==========================================================================
dcl-proc generar_nro_documento;
//==========================================================================
    dcl-pi *n packed(8:0);
    end-pi;

    dcl-s nrodoc packed(8:0);
    dcl-s existe packed(8:0);

        dow *on;
            exec sql    SELECT 
                      INT((RANDOM()*900000)+20000000)  INTO :nrodoc
                    FROM sysibm.sysdummy1;
        
            exec sql    SELECT  
                            COUNT(*) INTO :existe
                        FROM NOVPERDB.NPMP
                        WHERE
                            NRO_DOCUMENTO = : nrodoc;
            if existe = 0 ;
                leave;
            endif;
        enddo;

        return nrodoc;
end-proc;
//==========================================================================
dcl-proc generar_nombre;
//==========================================================================
    dcl-pi *n varchar(50);
    end-pi;

    dcl-s j                 packed(2:0);
    dcl-s primer_nombre     varchar(20);
    dcl-s segundo_nombre    varchar(20);
    dcl-s primer_apellido          varchar(20);

    dcl-s nombre_completo   varchar(50);

    exsr generar_index;
    primer_nombre = nombres(j);

    exsr generar_index;
    segundo_nombre = nombres(j);

    if primer_nombre = segundo_nombre;
        exsr generar_index;
        segundo_nombre = nombres(j);
    endif;

    exsr generar_index;
    primer_apellido = apellidos(j);

    nombre_completo =   primer_apellido
                        +' ' +primer_nombre
                        +' ' + segundo_nombre;                    

    return nombre_completo;

    //---------------------------------------------------------
    begsr generar_index;
    //---------------------------------------------------------
        exec sql    SELECT 
                    INT((RANDOM()*9)+1)  INTO :j
                FROM sysibm.sysdummy1;
    endsr;

    

end-proc;

//==========================================================================
dcl-proc generar_nro_aleatorios;
//==========================================================================
        dcl-pi *n       packed(15:0);
            min         packed(4:0) const;        
            max         packed(4:0) const;        
        end-pi;

        dcl-s rndnbr    packed(15:0);

        exec sql    SELECT 
                        INT((RANDOM()*:max)+:min)  INTO :rndnbr
                    FROM sysibm.sysdummy1;

        return  rndnbr;
end-proc;


//==========================================================================
dcl-proc generar_fecha;
//==========================================================================
    dcl-pi *n date;
        year        packed(4:0) const;        
        offset      packed(4:0) const;        
    end-pi;

    
    dcl-s anio         packed(4:0);
    dcl-s mes          packed(2:0);
    dcl-s dia          packed(2:0);
    dcl-s fecha_char   char(10);

    dcl-s nueva_fecha   date;

    exec sql    SELECT 
                    INT((RANDOM()*11)+1)  INTO :mes
                FROM sysibm.sysdummy1;

    exec sql    SELECT 
                    INT((RANDOM()*27)+1)  INTO :dia
                FROM sysibm.sysdummy1;
    exec sql    SELECT 
                    INT((RANDOM()* :offset )+1)  INTO :anio
                FROM sysibm.sysdummy1;                

    anio = anio + year;
    fecha_char = %char(anio) 
                + '-'
                + %trim(%editw( mes: '0  '))
                + '-'
                + %trim(%editw( dia: '0  '));

    nueva_fecha = %date(fecha_char:*ISO);

    return nueva_fecha;
end-proc;    
**
ROMERO
GOMEZ
PEREZ
DIEGUEZ
MESSI
DI MARIA
MARTINEZ
LOPEZ
ALE 
RODRIGUEZ
**
HUGO
PACO
LUIS
ROBERTO
MIGUEL
FRANCISCO
MARTIN
FEDERICO
RAMON
ALBERTO