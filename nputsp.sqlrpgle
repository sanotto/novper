**free
// NP: Novedades de Personal
// UT: Utilidades
// SP: Service Program
//
// Implementación

ctl-opt nomain;

//==========================================================================
dcl-proc generar_nro_aleatorios export;
//==========================================================================
        dcl-pi *n       packed(15:0);
            min         packed(4:0) const;        
            max         packed(4:0) const;        
        end-pi;

        dcl-s rndnbr    packed(15:0);

        exec sql    SELECT 
                        INT((RANDOM()*:max)+:min)  INTO :rndnbr
                    FROM sysibm.sysdummy1;

        return  rndnbr;
end-proc;

